# 文献紹介テンプレート

## 使い方

1. コマンドプロンプトを立ち上げる
2. `git clone https://f-ttok@bitbucket.org/f-ttok/latex-journalclub.git`
3. `cd latex-journalclub`
4. `latexmk main.tex` または VS Code (LaTeX Workshopをインストール済み)で`main.tex`をビルドする．

## フォントの変え方
日本語のフォントをNoto Serif JPにできます．
ついでに見出しが太めになります．
15, 22, 30, 35--37, 46行目を有効に（行頭の`%`を取る）して，21, 29, 32--34, 45行目をコメントに（行頭に`%`をつける）すると変わります．